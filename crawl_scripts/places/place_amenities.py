__author__ = 'carmenv'
import utils

class Amenity:
    def __init__(self, center_lat, center_lng, category, geodistance, name, type, lat, lng, requestnumber):
        self.center_lat = center_lat
        self.center_lng = center_lng
        self.category = category
        self.geodistance = geodistance
        self.name = name
        self.type = type
        self.lat = str(lat)
        self.lng = str(lng)
        self.requestnumber = requestnumber

class Place_amenities:
    def __init__(self,center_lat,center_lng):
        self.amenities = dict()
        self.amenities_final = dict()
        self.center_lat = center_lat
        self.center_lng = center_lng

    def add_amenity(self,category,name,type,geo_distance,latp,lngp,requestnumber):
        if category in self.amenities.keys():
            list_places = self.amenities[category]
            for namea,typea,geo_distancea,latpa,lngpa,requestnumbera in list_places:
                if namea == name or (latp==latpa and lngp==lngpa):
                    return
        utils.add_dict_list(self.amenities,category,(name,type,geo_distance,latp,lngp,requestnumber))
        self.amenities[category] = sorted(self.amenities[category], key=lambda tup: tup[2]) #places in ascending order by geo_distance

    def serialize_amenities(self):
        for category, places_list in self.amenities.iteritems():
            #print "%s" %len(places_list)
            for rank in range(0, len(places_list)):
                self.amenities_final[(category,rank)] = Amenity(self.center_lat, self.center_lng, category, places_list[rank][2], places_list[rank][0],places_list[rank][1], places_list[rank][3], places_list[rank][4], places_list[rank][5])
        return self.amenities_final