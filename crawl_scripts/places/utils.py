# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------
# Name:        utils.py
# Purpose:
#
# Author:      carmenv
#
# Created:     06/08/2013
# Copyright:   (c) carmenv 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import math
import unicodedata
import operator
import random
import string
import numpy as np
import sys
import urllib
import simplejson

from time import sleep
import csv
import re
import requests
import urllib2
import urllib
#from omgsecrets import MAPQUEST_APP_KEY

INFINITY = 100000000
EMPTY    = -1


############################################
# origins=Bobcaygeon+ON|41.43206,-81.38992
# origins=Bobcaygeon+ON|41.43206,-81.38992
############################################

def time_distance(mode,orig_coord_list,dest_coord_list):
    orig_coord = '|'.join(orig_coord_list)
    dest_coord = '|'.join(dest_coord_list)
    list_distances = []
    url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins="+orig_coord+"&destinations="+dest_coord+"&mode="+mode+"&language=en-EN&sensor=false"
    print url
    result= simplejson.load(urllib.urlopen(url))
    print result
    try:
        for distance_info in result['rows'][0]['elements']:
            if distance_info['status'] == 'OK':
                list_distances.append((distance_info['duration']['text'],str(distance_info['duration']['value']),distance_info['distance']['text'],str(distance_info['distance']['value'])))
            else:
                list_distances.append(('error','error','error','error'))
    except:
        print 'Error with distances API',result
        raise Exception("No results for distances API")
    return list_distances



def fixaccents(word):
    notAllowed = {u"\xb7":" ","\u2019":"-","$":"", "'":" ",";":" ", u"\xF2":'o',\
                  u"\xe3":u"a",u"�":u"a", u"�":u"a", u"�":u"a", u"�":u"e", u"�":u"e", u"�":u"e",u"�":u"i",u"�":"i",u"�":u"i",u"�":u"i",\
                  u"�":u"o", u"�":u"o", u"�":u"o", u"�":u"u",u"�":u"u", u"�":u"u",u"�":u"u",\
                  u"�":u"z",u"�":"C" ,u"�":u"n", u"\xf1":u"n",\
                  u"�":u"A",u"�":u"A",u"�":"A",u"�":"E",u"�":"E",u"�":"E",u"�":"I",u"�":"I",u"�":"I",u"�":"N",u"�":"O",u"�":"O",u"�":"O",u"�":"U",u"�":"U",u"�":"U",
                  u"\xb4":" ", u",":" ", u"\xb0":" " , u"\xf6": "o", u"\xae":"", u"\"":" "}
    for char in word:
        if char in notAllowed.keys():
            word = word.replace(char, notAllowed[char])

    "Returns a Unicode object on success, or None on failure"
    try:
       word = word.decode('utf-8')
    except:
        word = unicodedata.normalize('NFKD', word).encode('ASCII', 'ignore')
    return word

def add_dict_list(mydict,key,value):
    L = list()
    if key in mydict.keys():
        L = mydict[key]
    L.append(value)
    mydict[key] = L

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
    c = 2 * math.asin(math.sqrt(a))
    meters = 6371000.0 * c
    return meters


def make_request_google_places(lat,lng,category,api_key,rankby,radius=None):
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
    values = {'key' : api_key,
              'location' : lat+','+lng,
              'types' : category,
              'language' : 'en',
              'sensor' : 'false'
              }
    if rankby == 'prominence':
       values['radius'] = radius
    else:
       values['rankby'] = 'distance'

    arguments = urllib.urlencode(values)
    req = urllib2.Request(url+arguments)
    response = simplejson.load(urllib2.urlopen(req))
    if not response['status'] == "OK":
        #print '%s' %response
        raise Exception(response['status'])
    return response


def getnextpage_google_places(api_key,lat,lng,category,rankby,pagetoken,radius=None):
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
    values = {
              'key' : api_key,
              'location' : lat+','+lng,
              'types' : category,
              'pagetoken' : pagetoken,
              'language' : 'en',
              'sensor' : 'false',
              }

    if rankby == 'prominence':
       values['radius'] = radius
    else:
       values['rankby'] = 'distance'

    arguments = urllib.urlencode(values)
    req = urllib2.Request(url+arguments)
    response = simplejson.load(urllib2.urlopen(req))
    if not response['status'] == "OK":
        print '%s' %response
        raise Exception(response['status'])
    return response
