# -*- coding: latin-1 -*-
__author__ = 'carmenv'
from place_amenities import *
import time
import math
import utils
import pickle
import urllib
import urllib2
import simplejson

#############################################################################################################################
# Parameters: Google API key, list categories, cell center coordinates, cat translation, #requests counter
# Purpose:    Query the Google Places API in a radius of MAXIMUM_RADIUS from the center, uses pagination
# Output:     Amenities dictionary keyed by the category and the rank of the place (1st,3rd,5th closest)
#############################################################################################################################

def handle_over_query_limit(current_key_position):
    if current_key_position <(len(list_keys)-1):
                        current_key_position +=1
                        time.sleep(3)
                        api_key_google = list_keys[current_key_position]
                        print 'key changed api_key_google=%s' % api_key_google
    else:
                        print "waiting for one day"
                        time.sleep(86500)

def process_google_response(place):
                distance = utils.haversine(float(lng_center),float(lat_center),float(place['geometry']['location']['lng']),float(place['geometry']['location']['lat']))

                place_name = utils.fixaccents(place['name'])

                place_types = place['types']
                place_type = ''
                if len(place_types)>=2:
                    typecount =0
                    while (typecount < len(place_types)-1):
                        place_type = place_type + place_types[typecount] + '::'
                        typecount = typecount + 1
                    place_type = place_type + place_types[typecount]
                else:
                    place_type = place_types[0]

                return place_name, place_type, distance, place['geometry']['location']['lat'],place['geometry']['location']['lng']


def query_amenities(list_keys,typeslist,lat_center,lng_center, TOTAL_REQUESTS,api_key_google_last,current_key_position):
   zero_results = False
   zero_results_count = 0
   if api_key_google_last is None:
    api_key_google = list_keys[current_key_position]
   else:
    api_key_google = api_key_google_last

   pa = Place_amenities(lat_center,lng_center)

   for category in typeslist:
       print 'Processing category %s' % category
       _rankby = 'distance'
       maxradius = 8000
       continue_process = True
       while(continue_process):
           try:
                query_result = utils.make_request_google_places(lat_center,lng_center,category,api_key_google,_rankby,maxradius)
                TOTAL_REQUESTS +=1
                continue_process = False
                zero_results = False

                if TOTAL_REQUESTS >= 99840:
                    current_key_position,api_key_google = handle_over_query_limit(current_key_position)
                    TOTAL_REQUESTS = 0

           except Exception,detail:
                error =  str(detail)
                print '%s' % error
                if error.__contains__('INVALID_REQUEST'):
                    raise Exception(error)
                elif error.__contains__('ZERO_RESULTS'):
                    zero_results = True
                    zero_results_count +=1
                    continue_process = False
                elif error.__contains__('OVER_QUERY_LIMIT') and  error.__contains__('REQUEST_DENIED'):
                    current_key_position,api_key_google = handle_over_query_limit(current_key_position)
                    continue_process = True
                elif error.__contains__('HTTP Error 500: Internal Server Error'):
                    continue_process = True
                    continue
                elif error.__contains__('urlopen error [Errno -2]'):
                    continue_process = True
                    continue

                if zero_results:
                    if zero_results_count == 3:
                        return current_key_position,api_key_google,None,TOTAL_REQUESTS
                    time.sleep(1)
                    print 'here in zero_results %s' %zero_results_count
                    continue

           for place in query_result['results']:
                place_name,place_type,distance,place_lat,place_lng = process_google_response(place)
                pa.add_amenity(category,place_name,place_type,distance,place_lat,place_lng, TOTAL_REQUESTS)

           for pagecount in range(0,0):
                if query_result.get('next_page_token'):
                    nextpagetoken = query_result['next_page_token']
                    print '%s' %nextpagetoken
                    time.sleep(2)
                    try:
                        query_result = utils.getnextpage_google_places(api_key_google,lat_center,lng_center,category,_rankby,nextpagetoken,maxradius)
                        TOTAL_REQUESTS +=1

                        if TOTAL_REQUESTS >= 99840:
                            current_key_position,api_key_google = handle_over_query_limit(current_key_position)
                            TOTAL_REQUESTS = 0

                    except Exception,detail:
                        error =  str(detail)
                        print '%s' % error
                        if error.__contains__('INVALID_REQUEST'):
                            raise Exception(error)
                        elif error.__contains__('OVER_QUERY_LIMIT') and  error.__contains__('REQUEST_DENIED'):
                            current_key_position,api_key_google = handle_over_query_limit(current_key_position)

                    for place in query_result['results']:
                        place_name,place_type,distance,place_lat,place_lng = process_google_response(place)
                        pa.add_amenity(category,place_name,place_type,distance,place_lat,place_lng, TOTAL_REQUESTS)


                time.sleep(1)

   amenities_final = pa.serialize_amenities()
   return current_key_position,api_key_google,amenities_final,TOTAL_REQUESTS

list_keys = ["Place your API key here"]
lists_types = ['art_gallery|museum','book_store','library','movie_rental','movie_theater','night_club','bar|restaurant','stadium','amusement_park|rv_park|campground|zoo|aquarium','park','bus_station','taxi_stand','train_station|subway_station','bicycle_store','parking','gas_station','atm|bank','beauty_salon|hair_care|spa|gym','fire_station','police','doctor|dentist','hospital','bakery','cafe','shopping_mall|department_store','convenience_store|grocery_or_supermarket','clothing_store|shoe_store|jewelry_store','school|university','church|hindu_temple|mosque|place_of_worship|synagogue']
TOTAL_REQUESTS_PLACES = 0
TOTAL_REQUESTS_DISTANCE = 0
neighborhoods = ['Washington']
api_key_google_last = None
current_key_position = 0

for element in neighborhoods:
   try:
       famenities = open(element +'-amenities-rankbydistance.csv','w',1)
       input_file = open(element +'-mid-points.csv','r')
       for i, line_coordinates in enumerate(input_file):
               print 'Processing coordinates %s' % line_coordinates
               line_coordinates = line_coordinates.strip()
               lat_center,lng_center,i,j,k,sw_lat,sw_lng,ne_lat,ne_lng,empty = line_coordinates.split(',')
               if empty=='empty':
                   continue
               current_key_position,api_key_google_last,amenities_final,TOTAL_REQUESTS_PLACES  = query_amenities(list_keys,lists_types,lat_center,lng_center,TOTAL_REQUESTS_PLACES,api_key_google_last,current_key_position)
               if amenities_final is None:
                   continue
               sorted_amenities = sorted(amenities_final.keys())
               for category,rank in sorted_amenities:
                   place_details = amenities_final[(category, rank)]
                   famenities.write("%s,%s,%s,%s,%s,%s,%d,%.2f,%s,%s,%s,%s,%d\n" % (place_details.center_lat, place_details.center_lng,i,j,k, place_details.category,rank, place_details.geodistance, place_details.name,\
                       place_details.type, place_details.lat,place_details.lng, place_details.requestnumber))

   finally:
        famenities.close()
