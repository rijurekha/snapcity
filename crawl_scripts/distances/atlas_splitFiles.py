__auatlas__ = 'carmenv'

from subprocess import call, Popen

import time

count_requests = 0
count_files = 0

city  = 'Berlin-Atlas'
base_amenities_files = '/home/rijurekha/code/' + city + '/'
base_python = 'python'
base_code = "/home/rijurekha/code/"
atlas_servers = range(1,3)+range(4,8)
MAX_REQUESTS = 1200

script_file_name  = base_amenities_files+ city + "-server-requests.sh"
script_file     = open(script_file_name,'w')
files_created = []
fcurrent_name = base_amenities_files+city+str(count_files).zfill(2)+"-amenities.csv"
files_created.append(fcurrent_name)
fcurrent = open (fcurrent_name  ,'w')
script_file.write("ssh atlas"+str(atlas_servers[0]).zfill(2)+".mpi-sws.org "+ base_python+" "+base_code+"google_distances.py "+fcurrent_name+'\n')
list_commands = []

with open(base_amenities_files + city +'-amenities.csv','r') as input_file:
   for i, line in enumerate(input_file):
       line = line.strip()
       lat_center,lng_center,i,j,k,category,rank,geodistance,name,googlecategory,lat_place,lng_place = line.split(',')
       if count_requests >= MAX_REQUESTS:
           atlas_server = atlas_servers[count_files]
           count_requests = 0
           fcurrent.close()
           list_commands.append("ssh atlas"+str(atlas_server).zfill(2)+".mpi-sws.org "+base_python+" "+base_code+"google_distances.py "+fcurrent_name)
           count_files += 1
           fcurrent_name = base_amenities_files+city+str(count_files).zfill(2)+"-amenities.csv"
           fcurrent = open(fcurrent_name,'w')
           files_created.append(fcurrent_name)
           script_file.write("ssh atlas"+str(atlas_servers[count_files]).zfill(2)+".mpi-sws.org "+base_python+" "+base_code+"google_distances.py "+fcurrent_name+'\n')
       fcurrent.write(line+'\n')
       count_requests += 1

fcurrent.close()
if count_requests >0:
    atlas_server = atlas_servers[count_files]
    list_commands.append("ssh atlas"+str(atlas_server).zfill(2)+".mpi-sws.org "+base_python+" "+base_code+"google_distances.py "+fcurrent_name)

proc_list =[]
for element in list_commands:
    proc_list.append(Popen(element, shell=True))
    time.sleep(1)

for proc in proc_list:
    proc.communicate()

print files_created
cat_command = "cat "
for element in files_created:
    cat_command += element.replace('.csv','-distances.csv') +" "
print "> "+city+"-amenities-distances.csv"
script_file.write(cat_command+'\n')

script_file.close()
