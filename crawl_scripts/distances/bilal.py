import os
import sys
import traceback
from datetime import datetime
import time
from collections import defaultdict
from subprocess import call, Popen

MACHINES_FOR_CRAWLING = ["thor61", "thor62", "thor63", "thor64", "thor65", "thor66", "thor67", "thor68", "thor69", "thor70", "thor71", "thor72", "thor73", "thor74", "thor76"] #these machines must have virtual interfaces setup
INTERFACE_INIT = "lo"
INTERFACE_RANGE = [1,16] #both endpoints inclusive

CITY = "Rio"
ROOT_DIR = "/home/rijurekha/code/" #output and data folders will be stored here
PY_FILE = ROOT_DIR + "google_distances.py"
PART_FILES_DIR = ROOT_DIR + CITY + "/"


def get_machine_IP_list():

    machines_ips = set() # will have pairs like "machine_name IP"

    interfaces_to_use = []
    for i in range(INTERFACE_RANGE[0], INTERFACE_RANGE[-1] + 1):
        interfaces_to_use.append(INTERFACE_INIT + ":" + str(i))
    # print "Interfaces per machine to use: ", interfaces_to_use

    for machine in MACHINES_FOR_CRAWLING:
        cmd = "ssh %s /sbin/ifconfig" % machine
	print cmd
        interface_list = os.popen(cmd).read()

        # OUTPUT FORMAT
        # lo:14     Link encap:Local Loopback  
        #       inet addr:139.19.152.173  Mask:255.255.255.255
        #       UP LOOPBACK RUNNING  MTU:16436  Metric:1

        lines = interface_list.split("\n")
        for i in range(0, len(lines)-1):
            cur_line = lines[i]
            next_line = lines[i+1]

            for interface in interfaces_to_use:
                if interface in cur_line:
                    ip = next_line.split("inet addr:")[1].split("  ")[0]
                    machine_ip_pair = "%s %s" % (machine, ip)
                    machines_ips.add(machine_ip_pair)
                    break
        
    machines_ips = sorted(list(machines_ips))
    return machines_ips

def launch_jobs():

    machine_IP_list = get_machine_IP_list()
    
    try:
        fileIn = open("filelist")
        lines = fileIn.readlines()
        i = -1
        for line in lines:
            i+=1
            filename = PART_FILES_DIR +  line.strip()
            
            ind = i % len(machine_IP_list)
            machine_IP = machine_IP_list[ind].split(" ")
            machine_name = machine_IP[0]
            IP = machine_IP[1]

            print machine_name, IP, filename

            command = """ssh  %s BIND_ADDR="%s" LD_PRELOAD=/home/rijurekha/bind.so python %s %s """ % (machine_name, IP, PY_FILE, filename)
            
            
            print command , "\n\n"
            Popen(command, shell=True)
            time.sleep(1)
    except:
        traceback.print_exc()


def main():
    launch_jobs()


if __name__ == '__main__':
    main()
