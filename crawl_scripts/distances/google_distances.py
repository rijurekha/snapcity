# -*- coding: latin-1 -*-
__author__ = 'carmenv'
import sys
import time
import utils
import random


TOTAL_REQUESTS_PLACES = 0
TOTAL_REQUESTS_DISTANCE = 0

last_lat_center = None
if len(sys.argv) < 2:
    sys.stderr.write('Usage:'+ sys.argv[0] +'AMENITIES_FILENAME')

name_file_amenities = sys.argv[1]
print 'Calculating Google distances for %s' % name_file_amenities

name_file_distances = name_file_amenities.replace('.csv','-distances.csv')
fdistances = open(name_file_distances,'w',1)
time.sleep(1)
TOTAL_REQUESTS = 0
last_k = None
list_destinations = []
list_origin = []
last_lat_center = None
last_lng_center = None
list_i = []; list_j=[]; list_k=[]; list_category=[]; list_geodistance = []; list_name = []; list_googlecategory = [];
list_lat_place = [] ; list_lng_place=[]; list_rank=[];
try:
   with open(name_file_amenities,'r') as input_file:
       for i, line in enumerate(input_file):
           print 'Processing coordinates %s' % line,
           line = line.strip()
           lat_center,lng_center,i,j,k,category,rank,geodistance,name,googlecategory,lat_place,lng_place = line.split(',')
           if last_k and last_k!= k:
            list_origin = [last_lat_center+','+last_lng_center]
            distances_driving_list = utils.time_distance('driving',list_origin,list_destinations)
            TOTAL_REQUESTS +=1
            time.sleep(4+random.randint(0, 1))
            distances_walking_list = utils.time_distance('walking',list_origin,list_destinations)
            time.sleep(4+random.randint(0, 1))
            TOTAL_REQUESTS +=1
            print 'Writing distances for coordinates %s,%s,%s,%s,%s' % (last_lat_center,last_lng_center,last_i,last_j,last_k)
            for element in range(0,len(distances_driving_list)):
                distance_driving = distances_driving_list[element]
                try:
                    distance_walking = distances_walking_list[element]
                except:
                    distance_walking = ['error','error','error','error',]
                fdistances.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (last_lat_center, last_lng_center,last_i,last_j,last_k,list_category[element], \
               list_rank[element],list_geodistance[element],list_name[element],list_googlecategory[element],list_lat_place[element],list_lng_place[element],distance_driving[0],distance_driving[1],distance_driving[2],distance_driving[3],\
               distance_walking[0],distance_walking[1],distance_walking[2],distance_walking[3]))
            list_category=[]; list_geodistance = []; list_name = []; list_googlecategory = []; list_lat_place = [] ; list_lng_place=[]; list_rank=[];
            list_destinations = []; distances_driving_list =[]; distances_walking_list=[];

           list_category.append(category); list_geodistance.append(geodistance); list_name.append(name); list_googlecategory.append(googlecategory);
           list_lat_place.append(lat_place); list_lng_place.append(lng_place); list_rank.append(rank)
           list_destinations.append(lat_place+','+lng_place)
           last_k = k
           last_i = i
           last_j= j
           last_lat_center = lat_center
           last_lng_center = lng_center

   if int(last_k)>0:
    list_origin = [last_lat_center+','+last_lng_center]
    distances_driving_list = utils.time_distance('driving',list_origin,list_destinations)
    TOTAL_REQUESTS +=1
    time.sleep(4+random.randint(0, 1))
    distances_walking_list = utils.time_distance('walking',list_origin,list_destinations)
    time.sleep(4+random.randint(0, 1))
    TOTAL_REQUESTS +=1
    print 'Writing distances for coordinates %s,%s,%s,%s,%s' % (last_lat_center,last_lng_center,i,j,k)
    for element in range(0,len(distances_driving_list)):
        distance_driving = distances_driving_list[element]
        distance_walking = distances_walking_list[element]
        fdistances.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (last_lat_center, last_lng_center,last_i,last_j,last_k,list_category[element], \
       list_rank[element],list_geodistance[element],list_name[element],list_googlecategory[element],list_lat_place[element],list_lng_place[element],distance_driving[0],distance_driving[1],distance_driving[2],distance_driving[3],\
       distance_walking[0],distance_walking[1],distance_walking[2],distance_walking[3]))
    list_category=[]; list_geodistance = []; list_name = []; list_googlecategory = []; list_lat_place = [] ; list_lng_place=[]; list_rank=[];
    list_destinations = []

finally:
   fdistances.close()
   print 'Total distance requests for file %s = %d' % (name_file_amenities ,TOTAL_REQUESTS)
